if [ $# -lt 4 ]
then
    echo "Expected 4 argument, $# was given instead"
    echo "Expected format is:$./haproxy_install.sh <path/to/key> <loadbalancerIP> <ubuntuwebserverIP> <awslinuxwebseverIP>"
    exit 1
fi

key=$1
loadbalancer=$2

configlines=""

serverNo=1
# Gets the private ip of the webservers and generates config lines for each
private_ip=$(ssh -i $key ubuntu@$3 "hostname -I" 2>&1| xargs)
configlines+="server node$serverNo $private_ip:80 check
    "
# Launches website on the webserver
./webserver_install.sh $key $3 $serverNo

serverNo=2
# Gets the private ip of the webservers and generates config lines for each
private_ip=$(ssh -i $key ec2-user@$4 "hostname -I" 2>&1| xargs)
configlines+="server node$serverNo $private_ip:80 check
    "
# Launches website on the webserver
./webserver_install.sh $key $4 $serverNo

# installs haproxy on our load balancer if it isn't already installed
if ! output=$(ssh -o StrictHostKeyChecking=no -i $key ubuntu@$loadbalancer sudo haproxy -v)
then
    echo "haproxy could not be found. will install now"
    ssh -o StrictHostKeyChecking=no -i $key ubuntu@$loadbalancer '
    sudo apt update
    sudo apt install haproxy -y
    '
fi

# Writes config file including our backend ips to load balancing computer
ssh -o StrictHostKeyChecking=no -i $key ubuntu@$loadbalancer  << EOF
sudo bash -c 'echo "
defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
        errorfile 400 /etc/haproxy/errors/400.http
        errorfile 403 /etc/haproxy/errors/403.http
        errorfile 408 /etc/haproxy/errors/408.http
        errorfile 500 /etc/haproxy/errors/500.http
        errorfile 502 /etc/haproxy/errors/502.http
        errorfile 503 /etc/haproxy/errors/503.http
        errorfile 504 /etc/haproxy/errors/504.http

frontend haproxynode
    bind *:80
    mode http
    default_backend backendnodes

backend backendnodes
    balance roundrobin
    option forwardfor
    $configlines
" > /etc/haproxy/haproxy.cfg'
sudo systemctl restart haproxy
EOF

open http://$loadbalancer