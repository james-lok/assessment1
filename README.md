# Machine Requirements
For this assessment you will need to create repeat the create aws instructions for the following specs:
## 1. load balancer
* `image` - Ubuntu Server 18.04 LTS
* `instance requirements` - 1GB RAM | 1 vCPU 
* `name` - firstname_lb
* `project` - assessment1
* `security group` - http: anywhere| ssh: my ip
## 2. webserver 1
* `image` -Ubuntu Server 18.04 LTS
* `instance requirements` - 1GB RAM | 1 vCPU 
* `name` - firstname_lb
* `project` - assessment1
* `security group` - http: <load balancers private IP>| ssh: my ip
## 3. webserver 3
* `image` - Amazon Linux 2 AMI (hvm)
* `instance requirements` - 1GB RAM | 1 vCPU
* `name` - firstname_lb
* `project` - assessment1
* `security group` - http: <load balancers private IP>| ssh: my ip

*** NOTE: create loadbalancer first to get it's private IP for the webserver configuration ***

# Creating AWS Machines
1. Use the link to login (https://accounts.google.com/o/saml2/initsso?idpid=C03gyuhpv&spid=451567077141&forceauthn=false) to the aws dashboard, check the upper right corner to see if you are in the right role.
If you are not, press the small triangle next to it and select the correct role you wish to use.

2. In the search bar search for the service ec2 and press enter
3. Select the `image` you wish to use
4. Select the `instance` type whith the requirements you need
5. Configure instance details do not beed to be changed, click next.
6. Storage does not need to be changed, click next.
7. Add a tag with the key "Name" and the value of what you want the `name` of your machine to be, add a tag with key "Project" and enter the `project` name .
8. Create a new `security group` with you desired rules
9. Review your instance details and launch.
10. If you have an existing key pair choose it, tick the I acknowledge box after reading it and click launch instance
11. If you havent created the pair, select create a new key pair and pick a name for it, click download key pair and save it in your .ssh folder. Use this key pair for all machines created in this assessment
12. If this is the load balancing computer, ssh into it, get the private ip with the command 

```bash
$ sudo hostname -I
```
and copy it for later user

# Launching load balancer with website servers
## Prerequisites
* You have cloned this repository using:

```git clone git@bitbucket.org:garthj98/haproxy-loadbalancer.git```

* Have created the 3 machines listed in `Machine Requirements`

## Instructions

### 1. Make sure you are located in the directory of this repository

``` $ cd path/to/repo```

### 2. Type out the following command with your arguments
```bash
# Run the bashscipt with the arguments of the ssh key, the ip of your load balancing computer, and your ips of your webservers seperated by a space
$ proxy_install.sh <path/to/key> <loadBalancerIp> <ubuntuWebServerIp> <amazonLinuxWebServerIp>
```