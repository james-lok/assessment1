# Get onput from arguments of IP to use in the script
# The first argument passed to the script hsould b IP 

# extra make script second atgumetn become the key name or path 
# extra make scipt argument become user to login to machine

if [ $# != 3 ]
then
    echo "Expected 3 argument, $# was given instead"
    exit 1
fi

key=$1
hostname=$2


# first arg should be ubuntu
echo "number is $3"
if [[ "$3" == 1 ]];
then
instance="ubuntu"
package="nginx"
pm="apt"
elif [[ "$3" == 2 ]];
then
instance="ec2-user"
package="httpd"
pm="yum"
else
echo "else"
fi

if ! output=$(ssh -o StrictHostKeyChecking=no -i $key $instance@$hostname sudo $package -v)
then
    echo "$package could not be found"
    ssh -o StrictHostKeyChecking=no -i $key $instance@$hostname << EOF
    sudo $pm update -y
    sudo $pm install $package -y     
EOF
fi
scp -i  $key -r james_website $instance@$hostname:

serverName="Server$3"
ssh -o StrictHostKeyChecking=no -i $key $instance@$hostname << EOF
sudo sed 's/James/$serverName/' ~/james_website/template.html > ~/james_website/index.html
sudo mv ~/james_website/index.html /var/www/html/index.html
sudo service $package restart
EOF
